#pragma once
#include <iostream>
#include "cInputComponent.h"
class PlayerComponent : public InputComponent
{
public:
	PlayerComponent()
	{

	}
	virtual ~PlayerComponent() {};

	virtual void control(sf::Event::KeyEvent key) {
		switch (key.code)
		{
		case sf::Keyboard::W:
			std::cout << "hello" << std::endl;
			//send apply up force event to physics system
			break;
		case sf::Keyboard::A:
			//send apply left force event to physics system
			break;
		case sf::Keyboard::S:
			//send apply right force event to physics system
			break;
		case sf::Keyboard::D:
			//send apply down force event to physics system
			break;
		default:
			break;
		}
	};

private:
	void up();
	void down();
	void left();
	void right();

};