#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
// TestGUI used for GUIComponent and TESTGame
class TestGUI
{
public:
	// Create the label pointer here to reach it from OnButtonClick().
	sfg::Label::Ptr m_label;
	bool *isGUI;

	TestGUI(){};
	~TestGUI(){};

	std::shared_ptr<sfg::Window> CreateWindow()
	{
		//Initialise GUI
		// Create the label.
		m_label = sfg::Label::Create("Hello world!");

		// Create a simple button and connect the click signal.
		auto button = sfg::Button::Create("Greet SFGUI!");
		button->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TestGUI::OnButtonClick, this));

		// Create a vertical box layouter with 5 pixels spacing and add the label
		// and button to it.
		auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
		box->Pack(m_label);
		box->Pack(button, false);
		
		// Create a window and add the box layouter to it. Also set the window's title.
		auto gui = sfg::Window::Create();
		gui->SetTitle("Hello world!");
		gui->Add(box);
		gui->GetSignal(sfg::Window::OnMouseEnter).Connect(std::bind(&TestGUI::interact1, this, isGUI));
		gui->GetSignal(sfg::Window::OnMouseLeave).Connect(std::bind(&TestGUI::interact2, this, isGUI));
		return gui;
	}

	void OnButtonClick() {
		m_label->SetText("Hello SFGUI, pleased to meet you!");
	}

	void interact1(bool *isGUI)
	{
		*isGUI = true;
		std::cout << *isGUI << std::endl;
	}
	void interact2(bool *isGUI)
	{
		*isGUI = false;
		std::cout << *isGUI << std::endl;
	}
};