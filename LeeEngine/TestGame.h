#include "cGame.h"
#include "TestGUI.h"
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include "PlayerControl.h"

using namespace Lee;

class TestGame : public Game
{
private:
	b2Vec2 m_gravity;
	
	bool isGUI = false;
	bool lostFocus = false;
	b2PolygonShape* groundb2;
	b2PolygonShape* boxb2;
	b2CircleShape* circleb2;
protected:
	void CreateGround(float32 x, float32 y);
	void CreateBox(int32 mouseX, int32 mouseY);
	void CreatePlayer(int32 x, int32 y);
	void CreateCircle( float x, float y, float r);
	void CreateGUI();
public:
	TestGame() {}
	~TestGame() { }

	virtual void Init(Window& window);
	virtual void Update(float dt, Window& window);
	virtual void HandleInput(Window& window);
};

void TestGame::Init(Window& window) 
{
	// Setup Camera
	m_camera = new Camera(0,0,window.GetWidth(), window.GetHeight());
	window.SetCamera(m_camera);
	
	// Initialise Resources
	m_engine->m_res_manager->AddTexture("box", "./Resources/box.png");
	m_engine->m_res_manager->AddTexture("ground", "./Resources/ground.png");
	m_engine->m_res_manager->AddTexture("player", "./Resources/spritesheet.png");
	m_engine->m_res_manager->AddTexture("world", "./Resources/world.png");
	m_engine->m_res_manager->AddSound("footstep", "./Resources/footsteps-1.wav");
	m_engine->m_res_manager->AddSound("beep", "./Resources/beep.wav");
	groundb2 = new b2PolygonShape();
	groundb2->SetAsBox(Util::SF_TO_B2(512.f), Util::SF_TO_B2(8.f));
	boxb2 = new b2PolygonShape();
	boxb2->SetAsBox(Util::SF_TO_B2(16.f), Util::SF_TO_B2(16.f));
	circleb2 = new b2CircleShape();
	circleb2->m_radius = Util::SF_TO_B2(15.f);

	// set background
	Entity& bg = m_engine->getEntityManager()->create();
	bg.addComponent(new PhysicsComponent(0, 0));
	RenderableComponent* rend = new RenderableComponent("world");
	rend->SetWidth(4096.f);
	rend->SetHeight(1480.f);
	rend->SetType(RenderableComponent::RenderType::SPRITE);
	bg.addComponent(rend);
	bg.refresh();
	
	//Initialise GUI
	CreateGUI();
	CreateGround(100, 100);
	CreatePlayer(150, 150);
}

void TestGame::Update(float dt, Window& window)
{
	// Apply Game Logic here
	if (!isGUI && !lostFocus){
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right)){
			int mouseX = sf::Mouse::getPosition(window.GetWindow()).x;
			int mouseY = sf::Mouse::getPosition(window.GetWindow()).y;
			sf::Vector2f worldPos = window.GetWindow().mapPixelToCoords(sf::Vector2i(mouseX, mouseY));
			CreateBox(worldPos.x, worldPos.y);
			std::cout << m_engine->getEntityManager()->getEntityCount() << std::endl;
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)){
			int mouseX = sf::Mouse::getPosition(window.GetWindow()).x;
			int mouseY = sf::Mouse::getPosition(window.GetWindow()).y;
			sf::Vector2f worldPos = window.GetWindow().mapPixelToCoords(sf::Vector2i(mouseX, mouseY));
			CreateCircle(worldPos.x, worldPos.y, 15.f);
			std::cout << m_engine->getEntityManager()->getEntityCount() << std::endl;
		}
	}

	//m_world->DrawDebugData();
}

void TestGame::CreateGUI()
{
	TestGUI* gui = new TestGUI();
	gui->isGUI = &isGUI;
	Entity& gui_e = m_engine->getEntityManager()->create();
	gui_e.addComponent(new GUIComponent(gui->CreateWindow()));
	gui_e.setTag("Inventory");
	gui_e.refresh();
}

void TestGame::CreateGround(float32 x, float32 y)
{

	// Creates a box shape. Dive your desired width and height by 2
	b2FixtureDef fixture_def;
	fixture_def.density = 1.f; // Sets the density of the body
	fixture_def.friction = 0.7f;
	fixture_def.shape = groundb2; // Sets the shape

	Entity& ground = m_engine->getEntityManager()->create();
	RenderableComponent* rend = new RenderableComponent();
	rend->SetWidth(1024.f);
	rend->SetHeight(16.f);
	rend->SetColor(0, 255, 0, 70);
	rend->SetType(RenderableComponent::RenderType::RECTANGLE);
	ground.addComponent(rend);
	ground.addComponent(new PhysicsComponent(x, y, fixture_def, b2_staticBody, false));
	ground.refresh();
}
void TestGame::CreateBox(int32 mouseX, int32 mouseY)
{
	b2FixtureDef fixture_def;
	fixture_def.density = 1.f;
	fixture_def.friction = 0.7f;
	fixture_def.shape = boxb2;

	Entity& box = m_engine->getEntityManager()->create();
	RenderableComponent* rend = new RenderableComponent("box");
	rend->SetWidth(32);
	rend->SetHeight(32);
	rend->SetType(RenderableComponent::RenderType::SPRITE);
	box.addComponent(rend);
	box.addComponent(new PhysicsComponent(mouseX, mouseY, fixture_def, b2_dynamicBody, true));
	box.refresh();
}
void TestGame::CreatePlayer(int32 x, int32 y)
{
	b2FixtureDef fixture_def;
	fixture_def.density = 1.f;
	fixture_def.friction = 0.7f;
	fixture_def.shape = boxb2;

	Entity& box = m_engine->getEntityManager()->create();
	RenderableComponent* rend = new RenderableComponent("player");
	rend->SetWidth(32);
	rend->SetHeight(32);
	rend->SetType(RenderableComponent::RenderType::SPRITE);
	box.addComponent(rend);
	box.addComponent(new PhysicsComponent(x, y, fixture_def, b2_dynamicBody, false));
	box.addComponent(new InputComponent());
	box.setTag("player");
	box.refresh();
}
void TestGame::CreateCircle(float mouseX, float mouseY, float r) {
	b2FixtureDef cir_fix;
	cir_fix.density = 1.f;
	cir_fix.friction = 0.7f;
	cir_fix.shape = circleb2;

	Entity& cir = m_engine->getEntityManager()->create();

	RenderableComponent* rend = new RenderableComponent();
	rend->SetRadius(r);
	rend->SetColor(0, 0, 255, 255);
	rend->SetType(RenderableComponent::RenderType::CIRCLE);
	cir.addComponent(rend);
	cir.addComponent(new PhysicsComponent(mouseX, mouseY, cir_fix, b2_dynamicBody, true));
	cir.refresh();
}

void TestGame::HandleInput(Window& window)
{
	
	// check all the window's events that were triggered since the last iteration of the loop
	sf::Event event;
	auto physics = (PhysicsComponent*)m_engine->getTagManager()->getEntity("player").getComponent<PhysicsComponent>();
	while (window.GetWindow().pollEvent(event))
	{
		if (drawGUI)
			m_engine->SendEventToGUI(event);

		switch (event.type)
		{
		case sf::Event::Closed:
			window.GetWindow().close();
			break;
		case sf::Event::GainedFocus:
			lostFocus = false;
			return;
		case sf::Event::LostFocus:
			lostFocus = true;
			return;
		case sf::Event::Resized:
			// update the view to the new size of the window
			//sf::FloatRect visibleArea(0.f, 0.f, event.size.width, event.size.height);
			//m_camera->SetCentre(event.size.width / 2.f, event.size.height / 2.f);
			m_camera->SetSize(event.size.width, event.size.height);
			//m_camera->SetView(sf::View(visibleArea));
			//std::cout << visibleArea.width << std::endl;
			window.GetWindow().setView(*m_camera->GetView());
			break;

		case sf::Event::MouseWheelMoved:
			if (event.mouseWheel.delta > 0)
				m_camera->GetView()->zoom(0.9f);
			else
				m_camera->GetView()->zoom(1.1f);
			window.UpdateCamera();
			break;

		case sf::Event::KeyPressed:
			//if (!drawGUI)
			//m_engine->PassInput(event); // custom Input System
				switch (event.key.code)
				{
				case sf::Keyboard::G:
					drawGUI = !drawGUI;
					break;
				case sf::Keyboard::Right:
					m_camera->GetView()->move(sf::Vector2f(100.f, 0.f));
					window.UpdateCamera();
					break;
				case sf::Keyboard::Left:
					m_camera->GetView()->move(sf::Vector2f(-100.f, 0.f));
					window.UpdateCamera();
					break;
				case sf::Keyboard::Up:
					m_camera->GetView()->move(sf::Vector2f(0.f, -100.f));
					window.UpdateCamera();
					break;
				case sf::Keyboard::Down:
					m_camera->GetView()->move(sf::Vector2f(0.f, 100.f));
					window.UpdateCamera();
					break;
				case sf::Keyboard::W:
					m_engine->m_res_manager->PlaySound("beep");
					physics->m_body->ApplyForce(b2Vec2(0, -20), physics->m_body->GetWorldCenter(), true);
					break;
				case sf::Keyboard::A:
					m_engine->m_res_manager->PlaySound("beep");
					physics->m_body->ApplyForce(b2Vec2(-20, 0), physics->m_body->GetWorldCenter(), true);
					break;
				case sf::Keyboard::S:
					m_engine->m_res_manager->PlaySound("beep");
					physics->m_body->ApplyForce(b2Vec2(0, 20), physics->m_body->GetWorldCenter(), true);
					break;
				case sf::Keyboard::D:
					m_engine->m_res_manager->PlaySound("beep");
					physics->m_body->ApplyForce(b2Vec2(20, 0), physics->m_body->GetWorldCenter(), true);
					break;
				default:
					break;
				}
			break;
		case sf::Event::KeyReleased:
			switch (event.key.code)
			{
			case sf::Keyboard::W:
				physics->m_body->SetLinearVelocity(b2Vec2(0, 0));
				break;
			case sf::Keyboard::A:
				physics->m_body->SetLinearVelocity(b2Vec2(0, 0));
				break;
			case sf::Keyboard::S:
				physics->m_body->SetLinearVelocity(b2Vec2(0, 0));
				break;
			case sf::Keyboard::D:
				physics->m_body->SetLinearVelocity(b2Vec2(0, 0));
				break;
			default:
				break;
			}
			break;
		}
	}
}