#pragma once
#include <functional>
#include <Box2D\Box2D.h>
#include <SFML\Graphics.hpp>

using std::bind;

#define PI 3.141592653589793
#define RADTODEG 180/PI
#define DEGTORAD PI/180
#define SCALE 30.f

namespace Lee {
	struct Util
	{
		static sf::Vector2f B2_TO_SF(const b2Vec2 &vector, bool scale = true)
		{
			return sf::Vector2f(vector.x * (scale ? SCALE : 1.f), vector.y * (scale ? SCALE : 1.f));
		}
		
		static float B2_TO_SF(const float& num, bool scale = true)
		{
			return num * (scale ? SCALE : 1.f);
		}

		static b2Vec2 SF_TO_B2(const sf::Vector2f &vector, bool scale = true)
		{
			return b2Vec2(vector.x / (scale ? SCALE : 1.f), vector.y / (scale ? SCALE : 1.f));
		}

		static float SF_TO_B2(const float& num, bool scale = true)
		{
			return num / (scale ? SCALE : 1.f);
		}

		static sf::Color GLColorToSFML(const b2Color &color, sf::Uint8 alpha = 255)
		{
			return sf::Color(static_cast<sf::Uint8>(color.r * 255), static_cast<sf::Uint8>(color.g * 255), static_cast<sf::Uint8>(color.b * 255), alpha);
		}


	};
}