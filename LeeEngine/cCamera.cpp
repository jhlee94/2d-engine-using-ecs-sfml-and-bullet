#include "cCamera.h"
using namespace Lee;
Camera::Camera() {};
Camera::Camera(const float &center_x, const float &center_y, const float &width, const float &height)
{
	SetCentre(center_x, center_y);
	SetSize(width, height);
};
Camera::~Camera() {};

void Camera::SetCentre(const float &center_x, const float &center_y)
{
	m_view.setCenter(center_x, center_y);
	m_center_x = center_x;
	m_center_y = center_y;
};

void Camera::SetSize(const float &width, const float &height)
{
	m_view.setSize(width, height);
	m_width = width;
	m_height = height;
}

sf::View* Camera::GetView() { return &m_view; };

void Camera::SetView(const sf::View &view) {
	m_view = view;
};