#pragma once
#include <SFML\Graphics\View.hpp>

// Camera object that holds the view where all the Entities are drawn
namespace Lee
{
	class Camera
	{
	private:
		sf::View m_view; // main view
		float m_width, m_height, m_center_x, m_center_y;
	public:
		Camera();
		Camera(const float &center_x, const float &center_y, const float &width, const float &height);
		~Camera();

		// LookAt: Where the centre of view is
		void SetCentre(const float &center_x, const float &center_y);

		// Size of the view
		void SetSize(const float &width, const float &height);
		sf::View* GetView();
		void SetView(const sf::View &view);
	};
}