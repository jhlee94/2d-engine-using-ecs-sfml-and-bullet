#include "cDebugDrawer.h"

using namespace Lee;

DebugDrawer::~DebugDrawer()
{
}

void DebugDrawer::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	sf::ConvexShape polygon(vertexCount);
	sf::Vector2f center;
	for (int i = 0; i < vertexCount; i++)
	{
		sf::Vector2f transformedVec = Util::B2_TO_SF(vertices[i]);
		polygon.setPoint(i, sf::Vector2f(std::floor(transformedVec.x), std::floor(transformedVec.y))); // flooring the coords to fix distorted lines on flat surfaces
	}																								   // they still show up though.. but less frequently
	polygon.setOutlineThickness(-1.f);
	polygon.setFillColor(sf::Color::Transparent);
	polygon.setOutlineColor(Util::GLColorToSFML(color));

	m_window->GetWindow().draw(polygon);
}

void DebugDrawer::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	sf::ConvexShape polygon(vertexCount);
	for (int i = 0; i < vertexCount; i++)
	{
		sf::Vector2f transformedVec = Util::B2_TO_SF(vertices[i]);
		polygon.setPoint(i, sf::Vector2f(std::floor(transformedVec.x), std::floor(transformedVec.y))); // flooring the coords to fix distorted lines on flat surfaces
	}																								   // they still show up though.. but less frequently
	polygon.setOutlineThickness(-1.f);
	polygon.setFillColor(Util::GLColorToSFML(color, 60));
	polygon.setOutlineColor(Util::GLColorToSFML(color));

	m_window->GetWindow().draw(polygon);
}

void DebugDrawer::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
	sf::CircleShape circle(radius * SCALE);
	circle.setOrigin(radius * SCALE, radius * SCALE);
	circle.setPosition(Util::B2_TO_SF(center));
	circle.setFillColor(sf::Color::Transparent);
	circle.setOutlineThickness(-1.f);
	circle.setOutlineColor(Util::GLColorToSFML(color));

	m_window->GetWindow().draw(circle);
}

void DebugDrawer::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{
	sf::CircleShape circle(radius * SCALE);
	circle.setOrigin(radius * SCALE, radius * SCALE);
	circle.setPosition(Util::B2_TO_SF(center));
	circle.setFillColor(Util::GLColorToSFML(color, 60));
	circle.setOutlineThickness(1.f);
	circle.setOutlineColor(Util::GLColorToSFML(color));

	b2Vec2 endPoint = center + radius * axis;
	sf::Vertex line[2] =
	{
		sf::Vertex(Util::B2_TO_SF(center), Util::GLColorToSFML(color)),
		sf::Vertex(Util::B2_TO_SF(endPoint), Util::GLColorToSFML(color)),
	};

	m_window->GetWindow().draw(circle);
	m_window->GetWindow().draw(line, 2, sf::Lines);
}

void DebugDrawer::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
	sf::Vertex line[] =
	{
		sf::Vertex(Util::B2_TO_SF(p1), Util::GLColorToSFML(color)),
		sf::Vertex(Util::B2_TO_SF(p2), Util::GLColorToSFML(color))
	};

	m_window->GetWindow().draw(line, 2, sf::Lines);
}

void DebugDrawer::DrawTransform(const b2Transform& xf)
{
	float lineLength = 0.4;

	/*b2Vec2 xAxis(b2Vec2(xf.p.x + (lineLength * xf.q.c), xf.p.y + (lineLength * xf.q.s)));*/
	b2Vec2 xAxis = xf.p + lineLength * xf.q.GetXAxis();
	sf::Vertex redLine[] =
	{
		sf::Vertex(Util::B2_TO_SF(xf.p), sf::Color::Red),
		sf::Vertex(Util::B2_TO_SF(xAxis), sf::Color::Red)
	};

	// You might notice that the ordinate(Y axis) points downward unlike the one in Box2D testbed
	// That's because the ordinate in SFML coordinate system points downward while the OpenGL(testbed) points upward
	/*b2Vec2 yAxis(b2Vec2(xf.p.x + (lineLength * -xf.q.s), xf.p.y + (lineLength * xf.q.c)));*/
	b2Vec2 yAxis = xf.p + lineLength * xf.q.GetYAxis();
	sf::Vertex greenLine[] =
	{
		sf::Vertex(Util::B2_TO_SF(xf.p), sf::Color::Green),
		sf::Vertex(Util::B2_TO_SF(yAxis), sf::Color::Green)
	};

	m_window->GetWindow().draw(redLine, 2, sf::Lines);
	m_window->GetWindow().draw(greenLine, 2, sf::Lines);
}

void DebugDrawer::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color)
{

}

void DebugDrawer::DrawString(int x, int y, const char* string, ...)
{
	char buffer[128];
	va_list arg;
	_crt_va_start(arg, string);
	vsprintf_s(buffer, string, arg);
	_crt_va_end(arg);

	sf::Text text;
	
	text.setFont(font);
	text.setCharacterSize(15);
	text.setPosition(x,y);
	text.setString(buffer);
	text.setColor(sf::Color::Black);
	m_window->GetWindow().draw(text);
}

void DebugDrawer::DrawString(const b2Vec2& p, const char* string, ...)
{
	char buffer[128];
	va_list arg;
	_crt_va_start(arg, string);
	vsprintf_s(buffer, string, arg);
	_crt_va_end(arg);

	sf::Text text;

	text.setFont(font);
	text.setCharacterSize(15);
	text.setPosition(p.x, p.y);
	text.setString(buffer);
	text.setColor(sf::Color::Black);
	m_window->GetWindow().draw(text);
}

void DebugDrawer::DrawAABB(b2AABB* aabb, const b2Color& color)
{

}