#pragma once
#include <Box2D\Box2D.h>
#include <SFML\Graphics.hpp>
#include "cWindow.h"
#include "Util.h"

namespace Lee {
	class DebugDrawer : public b2Draw
	{
	private:
		Window* m_window;
		sf::Font font;
	public:
		DebugDrawer(Window* window) : m_window(window)
		{
			if (!font.loadFromFile("./Resources/arial.ttf"))
				std::cout << "font load error" << std::endl;
		};
		~DebugDrawer();

		void Create();
		void Destroy();

		void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);

		void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);

		void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);

		void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);

		void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);

		void DrawTransform(const b2Transform& xf);

		void DrawPoint(const b2Vec2& p, float32 size, const b2Color& color);

		void DrawString(int x, int y, const char* string, ...);

		void DrawString(const b2Vec2& p, const char* string, ...);

		void DrawAABB(b2AABB* aabb, const b2Color& color);
	};
}
