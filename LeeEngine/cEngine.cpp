#include "cEngine.h"

using namespace Lee;
Engine::Engine(Window* window, Game* game) : m_isRunning(false), m_window(window), m_game(game)
{
	m_res_manager = new ResourceManager();

	// set engine
	m_game->SetEngine(this);
	// FPS reset
	frame_count = 0;
	CalculateFPS();
	// Register Systems *order important*
	physics_sys = (PhysicsSystem*)getSystemManager()->setSystem(new PhysicsSystem(*m_window));
	rendering_sys = (RenderingSystem*)getSystemManager()->setSystem(new RenderingSystem(*m_window, m_res_manager));
	gui_sys = (GUISystem*)getSystemManager()->setSystem(new GUISystem(*m_window, m_res_manager, m_game->drawGUI));
	input_sys = (InputSystem*)getSystemManager()->setSystem(new InputSystem(*m_window));
	// initialise systems
	getSystemManager()->initializeAll();

	

	// initialise game/entities
	m_game->Init(*m_window);
	
}

void Engine::Start()
{
	if (m_isRunning)
		return;

	m_isRunning = true;

	while (m_isRunning)
	{
		CalculateFPS();
		loopStart();
		setDelta(1 / 60.f);

		m_game->HandleInput(*m_window);

		//m_game->HandleInput(*m_window);

		if (!m_window->IsOpen()){
			Stop();
			break;
		}

		m_window->UpdateCamera();

		// render the game on screen
		m_window->GetWindow().clear(sf::Color::Black);
		
		// Update Game Logic
		m_game->Update(1.0f / 60.f, *m_window);

		// Update System
		Update();

		//FPS String Draw
		m_window->GetWindow().setView(m_window->GetWindow().getDefaultView());
		sf::Vector2f world_pos = m_window->GetWindow().mapPixelToCoords(sf::Vector2i(5, 16));
		physics_sys->getDebugDrawer()->DrawString(world_pos.x, world_pos.y, "FPS: %5.2f", fps);

		m_window->GetWindow().display();
		
		//std::cout << fps << std::endl;
	}
}

void Engine::Update()
{
	physics_sys->process();
	rendering_sys->process();
	gui_sys->process();
}

void Engine::Stop()
{
	m_isRunning = false;

	delete m_game;
	m_game = nullptr;

	delete m_res_manager;
	m_res_manager = nullptr;
}

void Engine::CalculateFPS()
{
	//  Increase frame count
	frame_count++;

	//  Get the number of milliseconds since glutInit called
	//  (or first call to glutGet(GLUT ELAPSED TIME)).
	current_time = clock.getElapsedTime().asMilliseconds();
	//  Calculate time passed
	int timeInterval = current_time - previous_time;

	if (timeInterval > 1000)
	{
		//  calculate the number of frames per second
		fps = frame_count / (timeInterval / 1000.0f);

		//  Set time
		previous_time = current_time;
		//  Reset frame count
		frame_count = 0;
	}
}


void Engine::SendEventToGUI(sf::Event &event)
{
	gui_sys->ReceiveInputEvent(event);
}

void Engine::PassInput(sf::Event &event)
{
	input_sys->ReceiveInput(event);
	input_sys->process();
}