#pragma once
#include <SFML\System.hpp>
#include <Artemis\Artemis.h>
#include "cGame.h"
#include "cWindow.h"
#include "cResourceManager.h"
#include "cGUISystem.h"
#include "cRenderingSystem.h"
#include "cPhysicsSystem.h"
#include "cInputSystem.h"


namespace Lee{
	class Game;
	class Engine : public artemis::World
	{
	private:
		Game* m_game{ nullptr };
		bool m_isRunning;
		bool isFPS;
		sf::Clock clock;
	private:
		void CalculateFPS();
	protected:
		RenderingSystem* rendering_sys;
		PhysicsSystem* physics_sys;
		GUISystem* gui_sys;
		InputSystem* input_sys;
		int frame_count;
		float current_time, previous_time = 0;
	public:
		ResourceManager* m_res_manager;
		Window* m_window{ nullptr };
		float fps;
	public:
		Engine(Window* window, Game* game);
		void Start();
		void Update();
		void Stop();

		void ShowFPS(const bool& fps);
		void SendEventToGUI(sf::Event &event);
		void PassInput(sf::Event &event);
	};
}

