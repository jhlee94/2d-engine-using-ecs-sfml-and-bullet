#pragma once
#include "cEntityManager.h"
#include "cComponent.h"

namespace Lee{
	class cComponent;
	class cEntityManager;
	class cEntity
	{
	private:
		std::vector<std::unique_ptr<cComponent>> components;
		cEntityManager* owner{ nullptr };
		uint32_t m_uid;
		bool active;
	private:
		uint32_t GenerateUID();
		cEntity(const cEntity& other) {}
		void operator=(const cEntity& other) {}
	public:
		inline cEntity(uint32_t uid);
		virtual ~cEntity();

		void Update();

		void AddComponent(std::unique_ptr<cComponent> component);
		template<typename T>
		void RemoveComponent(T& comp);
		template <typename T>
		T *GetComponent(int ID);

		bool isActive() const;
		uint32_t GetUID() const;

	};
}