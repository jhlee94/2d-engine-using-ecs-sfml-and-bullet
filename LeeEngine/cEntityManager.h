#pragma once
#include <vector>
#include <memory>
#include "cEntity.h"

namespace Lee
{
	class cEntity;
	class cEngine;
	class cEntityManager
	{
	private:
		cEngine* engine;
		std::vector<std::unique_ptr<cEntity>> entities;
		std::vector < std::unique_ptr<cEntity>> available;

		uint32_t next_available_entity_id;
		int count;
		uint32_t uid;
		long total_created;
		long total_removed;

		cEntityManager(const cEntityManager &other) {}
		void operator=(const cEntityManager &other) {}
	public:
		cEntityManager();
		~cEntityManager();

		void AddEntity(cEntity& obj);
		void RemoveEntity(cEntity& obj);
		cEntity* CreateEntity();
		cEntity* GetEntity(uint32_t uid) const;

		void AddComponent(cEntity& obj, cComponent* comp);
		template<typename T>
		void RemoveComponent(cEntity& obj, T& comp);
		cComponent GetComponent(cEntity& obj, cComponent* comp);

		bool isActive(uint32_t uid);

		void Update();
		void RemoveAllEntities();

		int GetEntityCount();
		long GetTotalCreated();
		long GetTotalRemoved();
		
	};
}