#pragma once
#include <Artemis\Artemis.h>
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include "Util.h"
class GUIComponent : public artemis::Component
{
public:
	std::shared_ptr<sfg::Window> m_window;
	bool added = false;
public:
	GUIComponent(std::shared_ptr<sfg::Window> window)
	{
		m_window = window;
	}
	~GUIComponent() {};

};