#include "cGUISystem.h"
using namespace Lee;

GUISystem::GUISystem(Window& window, ResourceManager* res_manager, bool &draw)
{
	m_sfgui = new sfg::SFGUI();
	desktop = new sfg::Desktop();

	m_window = &window;
	m_res_manager = res_manager;
	addComponentType<GUIComponent>();
	drawGUI = &draw;
};

GUISystem::~GUISystem(){
	delete m_sfgui;
	delete desktop;
};

void GUISystem::initialize()
{
	guiMapper.init(*world);
}

void GUISystem::processEntities(artemis::ImmutableBag<artemis::Entity*> & bag)
{
	for (int i = 0; i < bag.getCount(); i++)
	{
		processEntity(*bag.get(i));
	}
	Draw(*m_window, *drawGUI);
}

void GUISystem::processEntity(artemis::Entity &e)
{
	GUIComponent* ep = guiMapper.get(e);
	if (!ep->added)
	{
		AddWindow(ep->m_window);
		ep->added = true;
	}
}

template<typename T>
void GUISystem::AddWindow(T window)
{
	desktop->Add(window);
}

void GUISystem::Draw(Window& window, bool &x)
{
	desktop->Update(1.0f);
	if (x)
		m_sfgui->Display(window.GetWindow());
}

void GUISystem::ReceiveInputEvent(sf::Event &event)
{
	desktop->HandleEvent(event);
}