#pragma once
#include <Artemis\Artemis.h>
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include "cGUIComponent.h"
#include "cResourceManager.h"
#include "cWindow.h"

// SFGUI handling system
// Users can make their own GUI window and
// add it to Entity using GUIComponent

namespace Lee {
	class GUISystem : public artemis::EntityProcessingSystem
	{
	private:
		artemis::ComponentMapper<GUIComponent> guiMapper;
		ResourceManager* m_res_manager;
		Window* m_window;
		bool *drawGUI;
	public:
		sfg::SFGUI *m_sfgui; // SFGUI desktop container
		sfg::Desktop *desktop; // SFGUI window container

		GUISystem(Window& window, ResourceManager* res_manager, bool &draw);
		virtual ~GUISystem();

		virtual void initialize();

		virtual void processEntities(artemis::ImmutableBag<artemis::Entity*> & bag);

		virtual void processEntity(artemis::Entity &e);

		template<typename T>
		void AddWindow(T window); // add window to desktop
		void Draw(Window& window, bool &x); // draw GUIs on the main window
		void ReceiveInputEvent(sf::Event &event); // catch input handling from the game

	};
}
