#pragma once
#include <Box2D\Box2D.h>
#include <Artemis\Artemis.h>
#pragma once
#include "cWindow.h"
#include "cEngine.h"

// Base Game Template Class
// Users must extend this class in order to create own game

using namespace artemis;
namespace Lee {
	class Engine;
	class Game
	{
	private:
		Game(Game& game) {}
		inline void operator=(Game& game) {}
		void CalculateFPS();
	protected:
		Engine* m_engine{ nullptr };
		Camera* m_camera{ nullptr };
	public:
		Game() { };
		virtual ~Game() { 
			delete m_camera; m_camera = nullptr;
		}

		// Initialise Game Resources
		virtual void Init(Window& window) {};
		// Update Game (Logic)
		virtual void Update(float dt, Window& window) {};
		// Handle Input from user
		virtual void HandleInput(Window& window) {};

		// Set game's engine
		void SetEngine(Engine* engine) { m_engine = engine; };

		bool drawGUI = false;
	};
}
