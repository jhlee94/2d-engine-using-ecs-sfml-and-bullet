#pragma once
#include "cWindow.h"
#include "cInputComponent.h"
/* not completed */
// Input System
// Takes sf::Event from the main window and
// processes each key according to Entity's game input logic
namespace Lee {
	class InputSystem : public artemis::EntityProcessingSystem
	{
	private:
		artemis::ComponentMapper<InputComponent> inputMapper;
		Window* m_window;
		bool *drawGUI;
		sf::Event *m_event = nullptr;
	public:
		InputSystem(Lee::Window& window)
		{
			m_window = &window;
			addComponentType<InputComponent>();
		};
		virtual ~InputSystem(){
		};

		virtual void initialize()
		{
			inputMapper.init(*world);
		}

		virtual void processEntities(artemis::ImmutableBag<artemis::Entity*> & bag)
		{
				for (int i = 0; i < bag.getCount(); i++)
				{
					processEntity(*bag.get(i), *m_event);
				}
		}

		virtual void processEntity(artemis::Entity &e)
		{

		}

		virtual void processEntity(artemis::Entity &e, sf::Event &event)
		{			
			inputMapper.get(e)->control(event.key, e);
		}

		// Event Function
		void ReceiveInput(sf::Event &event)
		{
			m_event = &event;
		}
	};
}
