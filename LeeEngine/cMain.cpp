#include "cWindow.h"
#include "cEngine.h"
#include "TestGame.h"

using namespace Lee;

// Example of setting up Engine and custom Game
int main()
{
	TestGame* game = new TestGame();
	Window window(1024, 768, "2D Game Engine");
	window.SetVerticleSync(true);
	Engine engine(&window, game);
	engine.Start();

	return 0;
}