#pragma once
#include <Box2D\Box2D.h>
#include "Util.h"
class PhysicsComponent : public artemis::Component
{
	enum PhysicsShape
	{
		LINE,
		TRIANGLE,
		RECTANGLE,
		CIRCLE,
		POLYGON
	};
public:
	b2Body* m_body{ nullptr };
	b2BodyDef m_body_def;
	b2FixtureDef m_fix_def;
	sf::Vector2f position;
	bool spawned = false;
	bool isRotatable = true;
public:
	PhysicsComponent(float32 x, float32 y, b2FixtureDef &fix_def, b2BodyType type, bool rotate)
	{
		m_fix_def = fix_def;
		m_body_def.position = Lee::Util::SF_TO_B2(sf::Vector2f(x, y));
		m_body_def.type = type;		
		isRotatable = rotate;
	}
	PhysicsComponent(float32 x, float32 y)
	{
		position.x = x;
		position.y = y;
		spawned = true;
	}

};