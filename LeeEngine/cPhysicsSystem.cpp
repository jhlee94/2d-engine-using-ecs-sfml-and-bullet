#include "cPhysicsSystem.h"
using namespace Lee;

PhysicsSystem::PhysicsSystem(Window& window) : m_window(window)
{
	// Prepare Box2D World
	m_world = new b2World(b2Vec2_zero);
	//m_gravity = b2Vec2(0.f, 9.8f);
	m_world->SetWarmStarting(true);
	m_world->SetAllowSleeping(true);
	//m_world->SetGravity(m_gravity);
	debug_drawer = new DebugDrawer(&m_window);
	// Register DebugDrawer 
	m_world->SetDebugDraw(debug_drawer);
	//debug_drawer->SetFlags(b2Draw::e_shapeBit);

	addComponentType<PhysicsComponent>();
};
PhysicsSystem::~PhysicsSystem()
{
	delete m_world;
	m_world = nullptr;
	delete debug_drawer;
	debug_drawer = nullptr;
}

void PhysicsSystem::initialize()
{
	physicsMapper.init(*world);
	//window_body = CreateBody(0.f, 0.f, m_window.GetWidth()/SCALE, m_window.GetHeight()/SCALE, b2_staticBody);
	//window_body->SetFixedRotation(true);
};

void PhysicsSystem::processEntities(artemis::ImmutableBag<artemis::Entity*> & bag)
{
	///** Simulate the world */
	m_world->Step(world->getDelta(), 8, 5);
	//std::cout << "Collision Count: " << m_world->GetContactCount() << std::endl;
	for (int i = 0; i < bag.getCount(); i++)
	{
		processEntity(*bag.get(i));
	}
}

void PhysicsSystem::processEntity(artemis::Entity &e)
{
	PhysicsComponent* ep = physicsMapper.get(e);
	if (!ep->spawned)
	{
		// if the body hasn't been added to the world yet
		// then add body and set spwaned = true
		ep->m_body = m_world->CreateBody(&ep->m_body_def);
		ep->m_body->CreateFixture(&ep->m_fix_def);
		ep->spawned = true;
		ep->m_body->SetFixedRotation(!ep->isRotatable); // Rotatable?
	}
};

// For Debug Purpose
DebugDrawer* PhysicsSystem::getDebugDrawer() const { return debug_drawer; }