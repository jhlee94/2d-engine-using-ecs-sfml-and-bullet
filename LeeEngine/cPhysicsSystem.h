#pragma once
#include <Artemis\Artemis.h>
#include <Box2D\Box2D.h>
#include "cPhysicsComponent.h"
#include "cWindow.h"
#include "Util.h"
#include "cDebugDrawer.h"

// Physics System using Artemis ECS and Box2D
// System holds the Box2D worlds and body components from entities
namespace Lee {
	class PhysicsSystem : public artemis::EntityProcessingSystem
	{
	private:
		artemis::ComponentMapper<PhysicsComponent>		physicsMapper;
		b2World* m_world; // main Box2D world
		b2Body* window_body{ nullptr };
		Window& m_window;
		DebugDrawer* debug_drawer;
	public:
		PhysicsSystem(Window& window);
		~PhysicsSystem();
		//initialise the system
		virtual void initialize();

		// step the world and update body positions
		virtual void processEntities(artemis::ImmutableBag<artemis::Entity*> & bag);

		// add entity to the world if it hasn't been added
		virtual void processEntity(artemis::Entity &e);
		DebugDrawer* getDebugDrawer() const;
	};
}