#pragma once
#include "Animation.hpp"
#include <string>
class RenderableComponent : public artemis::Component
{
private:
	//sf::Texture m_texture;

public:
	enum RenderType
	{
		LINE,
		TRIANGLE,
		RECTANGLE,
		CIRCLE,
		POLYGON,
		SPRITE,
		TEXT
	};

	std::string m_tex_name;
	sf::Vector2f* m_vertices;
	sf::Color m_color;
	float width, height;
	float radius;
	int size;
	RenderType m_type;
	Animation* animations{ nullptr };
	bool isAnimated = false;

public:
	RenderableComponent(std::string tex_name = "") : m_tex_name(tex_name) {
		m_color = sf::Color::Black;
		width = 0.f;
		height = 0.f;
		radius = 0.f;
		int size = 0;
	};
	virtual ~RenderableComponent() { delete m_vertices; m_vertices = nullptr; }

	void SetType(const RenderType& type)
	{
		m_type = type;
	}

	void SetWidth(const float& w)
	{
		width = w;
	}

	void SetHeight(const float& h)
	{
		height = h;
	}
	
	void SetRadius(const float& r)
	{
		radius = r;
	}

	void SetAnimations(Animation *animation)
	{
		isAnimated = true;
		animations = animation;
	}

	void SetVerticles(sf::Vector2f* vertices, int size)
	{
		if (size == 2)
		{
			m_type = LINE;
		}
		else if (size == 3)
		{
			m_type = TRIANGLE;
		}
		else
		{
			m_type = POLYGON;
		}
		m_vertices = vertices;
	}

	void SetColor(const float& r, const float& g, const float& b, const float& a = 1)
	{
		m_color = sf::Color(r, g, b, a);
	}
};

