#include "cRenderingSystem.h"
using namespace Lee;

RenderingSystem::RenderingSystem(Window& window, ResourceManager* res_manager)
{
	m_window = &window;
	m_res_manager = res_manager;
	addComponentType<RenderableComponent>();
	addComponentType<PhysicsComponent>();
};

RenderingSystem::~RenderingSystem()
{
}

// Initialise Rendering System
 void RenderingSystem::initialize()
{
	renderableMapper.init(*world);
	physicsMapper.init(*world);
};

 // Render current entities
void RenderingSystem::processEntities(artemis::ImmutableBag<artemis::Entity*> & bag)
{
	for (int i = 0; i < bag.getCount(); i++)
	{
		processEntity(*bag.get(i));
	}
}

// Render an entity depending on its component data
void RenderingSystem::processEntity(artemis::Entity &e)
{
	sf::RectangleShape rect;
	sf::CircleShape circ;
	sf::ConvexShape convex;
	sf::Sprite sprite;
	sf::Vector2f sfPos;
	float angle;

	if (physicsMapper.get(e)->m_body != nullptr){
		sfPos = Util::B2_TO_SF(physicsMapper.get(e)->m_body->GetPosition());
		angle = physicsMapper.get(e)->m_body->GetAngle() * RADTODEG;
	}
	else {
		sfPos = physicsMapper.get(e)->position;
		angle = 0.f;
	}

	switch (renderableMapper.get(e)->m_type)
	{
	case RenderableComponent::RenderType::LINE:
		break;
	case RenderableComponent::RenderType::RECTANGLE:
		rect = DrawRect(sfPos.x, sfPos.y,
			renderableMapper.get(e)->width, renderableMapper.get(e)->height,
			angle,
			renderableMapper.get(e)->m_tex_name, renderableMapper.get(e)->m_color);
		m_window->GetWindow().draw(rect);
		break;
	case RenderableComponent::RenderType::CIRCLE:
		circ = DrawCircle(renderableMapper.get(e)->radius,
			sfPos.x, sfPos.y,
			angle,
			renderableMapper.get(e)->m_tex_name, renderableMapper.get(e)->m_color);
		m_window->GetWindow().draw(circ);
		break;
	case RenderableComponent::RenderType::TRIANGLE:
		convex = DrawTriangle(renderableMapper.get(e)->m_vertices, sfPos.x,
			sfPos.y, angle,
			renderableMapper.get(e)->m_tex_name, renderableMapper.get(e)->m_color);
		m_window->GetWindow().draw(convex);
		break;
	case RenderableComponent::RenderType::POLYGON:
		convex = DrawPolygon(renderableMapper.get(e)->m_vertices, renderableMapper.get(e)->size,
			sfPos.x, sfPos.y, angle,
			renderableMapper.get(e)->m_tex_name, renderableMapper.get(e)->m_color);
		m_window->GetWindow().draw(convex);
		break;
	case RenderableComponent::RenderType::SPRITE:
		if (!renderableMapper.get(e)->isAnimated) {
			sprite = DrawSprite(renderableMapper.get(e)->width, renderableMapper.get(e)->height,
				sfPos.x, sfPos.y,
				angle, renderableMapper.get(e)->m_tex_name);
			m_window->GetWindow().draw(sprite);
		}
		else {
			AnimatedSprite animatedSprite(sf::seconds(0.2), true, false);
			animatedSprite.setPosition(sfPos);
		}
		break;
	default:
		break;
	}
};

// Simple Drawing Functions

sf::CircleShape RenderingSystem::DrawCircle(float radius, float x, float y, float angle, std::string tex_name, sf::Color color)
{
	sf::CircleShape shape(radius);
	if (tex_name != "")
	{
		shape.setTexture(&m_res_manager->GetTexture(tex_name));
	}
	else {
		shape.setFillColor(color);
	}
	shape.setPosition(x, y);
	shape.setOrigin(radius, radius);
	shape.setRotation(angle);

	return shape;
}

sf::RectangleShape RenderingSystem::DrawRect(float x, float y, float width, float height, float angle, std::string tex_name, sf::Color color)
{
	sf::RectangleShape shape(sf::Vector2f(width, height));
	if (tex_name != "")
	{
		shape.setTexture(&m_res_manager->GetTexture(tex_name));
	}
	else {
		shape.setFillColor(color);
	}
	shape.setPosition(x, y);
	shape.setOrigin(width / 2.f, height / 2.f);
	shape.setRotation(angle);

	return shape;
}

sf::ConvexShape RenderingSystem::DrawTriangle(const sf::Vector2f* vertices, float x, float y, float angle, std::string tex_name, sf::Color color)
{
	sf::ConvexShape shape(3);
	for (unsigned int i = 0; i < 3; i++)
	{
		shape.setPoint(i, vertices[i]);
	}

	if (tex_name != "")
	{
		shape.setTexture(&m_res_manager->GetTexture(tex_name));
	}
	else {
		shape.setFillColor(color);
	}

	shape.setPosition(x, y);
	//shape.setOrigin(renderableMapper.get(e)->width / 2.f, renderableMapper.get(e)->height / 2.f);
	shape.setRotation(angle);
	return shape;
}

sf::ConvexShape RenderingSystem::DrawPolygon(const sf::Vector2f* vertices, int vertexCount, float x, float y, float angle, std::string tex_name, sf::Color color)
{
	sf::ConvexShape convex(vertexCount);
	for (unsigned int i = 0; i < vertexCount; i++)
	{
		convex.setPoint(i, vertices[i]);
	}

	if (tex_name != "")
	{
		convex.setTexture(&m_res_manager->GetTexture(tex_name));
	}
	else {
		convex.setFillColor(color);
	}
	convex.setPosition(x, y);
	//convex.setOrigin(width / 2.f, renderableMapper.get(e)->height / 2.f);
	convex.setRotation(angle);

	return convex;
}

sf::Sprite RenderingSystem::DrawSprite(float width, float height, float x, float y, float angle, std::string tex_name)
{
	sf::Sprite sprite;
	sprite.setTextureRect(sf::IntRect(0, 0, width, height));
	sprite.setTexture(m_res_manager->GetTexture(tex_name));
	sprite.setPosition(x, y);
	sprite.setOrigin(width / 2.f, height / 2.f);
	sprite.setRotation(angle);

	return sprite;
}