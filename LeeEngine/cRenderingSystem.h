#pragma once
#include <Artemis\Artemis.h>
#include "cWindow.h"
#include "cResourceManager.h"
#include "Animation.hpp"
#include "AnimatedSprite.hpp"
#include "cPhysicsComponent.h"
#include "cRenderableComponent.h"

// Rendering System using SFML

namespace Lee {
	class RenderingSystem : public artemis::EntityProcessingSystem
	{
	private:
		artemis::ComponentMapper<RenderableComponent>		renderableMapper;
		artemis::ComponentMapper<PhysicsComponent>		physicsMapper;
		ResourceManager* m_res_manager;
		Window* m_window;

	public:
		RenderingSystem(Window& window, ResourceManager* res_manager);
		~RenderingSystem();
		// Initialise Rendering System
		virtual void initialize();
		// Render current entities
		virtual void processEntities(artemis::ImmutableBag<artemis::Entity*> & bag);
		// Render an entity
		virtual void processEntity(artemis::Entity &e);

		// Simple Drawing Functions
		sf::RectangleShape DrawRect(float x, float y, float width, float height, float angle, std::string tex_name, sf::Color color);
		sf::CircleShape DrawCircle(float radius, float x, float y, float angle, std::string tex_name, sf::Color color);
		sf::ConvexShape DrawTriangle(const sf::Vector2f* vertices, float x, float y, float angle, std::string tex_name, sf::Color color);
		sf::ConvexShape DrawPolygon(const sf::Vector2f* vertices, int vertexCount, float x, float y, float angle, std::string tex_name, sf::Color color);
		sf::Sprite DrawSprite(float width, float height, float x, float y, float angle, std::string tex_name);
	};
}