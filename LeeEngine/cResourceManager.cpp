#include "cResourceManager.h"

using namespace Lee;
ResourceManager::ResourceManager()
{
}


ResourceManager::~ResourceManager()
{
	RemoveAll();
}

void ResourceManager::AddTexture(const std::string& tex_name, const std::string& filepath)
{
	sf::Texture texture;
	if (texture.loadFromFile(filepath)) {
		texture.setSmooth(true);
		texture_map.emplace(tex_name, texture);
	}
	else {
		// Error...
		std::cout << "Texture doesn't Exist" << std::endl;
	}
}


void ResourceManager::AddFont(const std::string& font_name, const std::string& filepath)
{
	sf::Font font;
	if (font.loadFromFile(filepath))
	{
		font_map.emplace(font_name, font);
	}
	else
	{
		std::cout << "Font doesn't Exist" << std::endl;
	}
}
void ResourceManager::AddSound(const std::string& sound_name, const std::string& filepath)
{
	sf::SoundBuffer *buffer = new sf::SoundBuffer();
	if (buffer->loadFromFile(filepath))
	{
		sound_map.emplace(sound_name, buffer);
	}
	else{
		std::cout << "Sound doesn't Exist" << std::endl;
	}
}
void ResourceManager::AddMusic(const std::string& music_name, const std::string& filepath)
{

}

void ResourceManager::RemoveTexture(const std::string& tex_name)
{
	if (!texture_map.erase(tex_name))
	{
		std::cout << "Texture doesn't Exist" << std::endl;
	};
}
void ResourceManager::RemoveFont(const std::string& font_name)
{
	if (!font_map.erase(font_name))
	{
		std::cout << "Font doesn't Exist" << std::endl;
	};
}
void ResourceManager::RemoveSound(const std::string& sound_name)
{
	if (!sound_map.erase(sound_name))
	{
		std::cout << "Sound doesn't Exist" << std::endl;
	};
}
void ResourceManager::RemoveMusic(const std::string& music_name)
{
}

void ResourceManager::RemoveAll()
{
	texture_map.clear();
	font_map.clear();
	sound_map.clear();
}

sf::Texture& ResourceManager::GetTexture(const std::string& tex_name)
{
	auto search = texture_map.find(tex_name);
	if (search != texture_map.end())
	{
		// Texture exists
		return search->second;
	}
	else {
	}
}

sf::Font& ResourceManager::GetFont(const std::string& font_name)
{
	auto search = font_map.find(font_name);
	if (search != font_map.end())
	{
		// Texture exists
		return search->second;
	}
	else {
	}
}
sf::SoundBuffer* ResourceManager::GetSound(const std::string& sound_name)
{
	auto search = sound_map.find(sound_name);
	if (search != sound_map.end())
	{
		return search->second;
	}
	else {
	}
}
sf::Music& ResourceManager::GetMusic(const std::string& music_name)
{

}

void ResourceManager::PlaySound(const std::string& sound_name)
{
	if (playingSounds.size() == 0)
	{
		playingSounds.push_back(sf::Sound());
		playingSounds.at(0).setBuffer(*GetSound(sound_name));
		playingSounds.at(0).play();
	}
	else
	{
		int location = -1;
		for (int i = 0; i < playingSounds.size(); i++)
		{
			if (playingSounds.at(i).getStatus() != sf::Sound::Playing && location == -1)
			{
				location = i;
			}
		}

		if (location != -1)
		{
			playingSounds.at(location).setBuffer(*GetSound(sound_name));
			playingSounds.at(location).play();
		}
		else
		{
			playingSounds.push_back(sf::Sound());
			playingSounds.at(playingSounds.size() - 1).setBuffer(*GetSound(sound_name));
			playingSounds.at(playingSounds.size() - 1).play();
		}

	}
}