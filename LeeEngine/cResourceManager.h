#pragma once
#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <map>
namespace Lee {
	class ResourceManager
	{
	private:
		std::map<std::string, sf::Texture> texture_map;
		std::map<std::string, sf::Font> font_map;
		std::map<std::string, sf::SoundBuffer*> sound_map;
		//std::map<std::string, sf::Music> music_map;
		std::vector<sf::Sound> playingSounds;
	public:
		ResourceManager();
		virtual ~ResourceManager();

		void AddTexture(const std::string& tex_name, const std::string& filepath);
		void AddFont(const std::string& font_name, const std::string& filepath);
		void AddSound(const std::string& sound_name, const std::string& filepath);
		void AddMusic(const std::string& music_name, const std::string& filepath);

		void RemoveTexture(const std::string& tex_name);
		void RemoveFont(const std::string& font_name);
		void RemoveSound(const std::string& sound_name);
		void RemoveMusic(const std::string& music_name);

		void RemoveAll();

		sf::Texture& GetTexture(const std::string& tex_name);
		sf::Font& GetFont(const std::string& font_name);
		sf::SoundBuffer* GetSound(const std::string& sound_name);
		sf::Music& GetMusic(const std::string& music_name);

		void PlaySound(const std::string& sound_name);
	};
}
