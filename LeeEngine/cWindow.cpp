#include "cWindow.h"

using namespace Lee;
Window::Window(int width, int height, const std::string& title) :
m_width(width), m_height(height), m_title(title)
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 4;
	// Prepare the window
	m_window = new sf::RenderWindow(sf::VideoMode(width, height), title, sf::Style::Default, settings);
	this->SetVerticleSync(true);
}
Window::~Window()
{
	delete m_window;
	m_window = nullptr;
}
