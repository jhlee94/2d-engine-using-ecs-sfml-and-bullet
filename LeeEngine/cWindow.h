#pragma once
#include <SFML\Graphics.hpp>
#include <iostream>
#include "cCamera.h"

// Wrap up window class for sf::RenderWindow
// combined by using custom camera object

namespace Lee{
	class Window
	{
	private:
		int m_width;
		int m_height;
		std::string m_title;
		sf::RenderWindow* m_window{ nullptr }; // main window
		Camera* m_camera{ nullptr }; // Camera = main view
		Window(const Window& other) {}
		void operator=(const Window& other) {}
	protected:
	public:
		Window(int width, int height, const std::string& title);
		virtual ~Window();
		
		inline bool IsOpen() const
		{
			return m_window->isOpen();
		}
		inline sf::RenderWindow& GetWindow() const
		{
			return *m_window;
		}
		inline int GetWidth() const
		{
			return m_width;
		}
		inline int GetHeight() const
		{
			return m_height;
		}
		inline float GetRatio() const
		{
			return (float)m_width / (float)m_height;
		}
		inline sf::Vector2f GetCentre() const
		{
			return sf::Vector2f((float)m_width / 2.f, (float)m_height / 2.f);
		}
		inline const std::string& GetTitle() const
		{
			return m_title;
		}
		inline void SetVerticleSync(const bool& a)
		{
			m_window->setVerticalSyncEnabled(a);
		}
		inline void SetCamera(Camera* camera)
		{
			m_camera = camera;
			m_window->setView(*m_camera->GetView());
		}
		inline void UpdateCamera()
		{
			m_window->setView(*m_camera->GetView());
		}
	};
}